from flask import render_template, request, redirect, jsonify

from . import app
from .models import Recipe, Ingredient, RecipeIngredientAssoc
from . import db
from .utils import get_all_recipes


@app.get("/")
@app.get("/recipes")
def index():
    data = {"recipes": get_all_recipes()}
    return render_template("pages/index.html", data=data)


@app.route("/ingredients")
@app.route("/sastojci")
def show_ingredients():
    ingredients = db.session.query(Ingredient).all()
    data = {"ingredients": ingredients}
    return render_template("pages/ingredients.html", data=data)


@app.get("/ingredients/new")
def show_new_ingredient_form():
    return render_template("pages/add_ingredient.html")


@app.post("/ingredients/new")
def add_new_ingredient():
    name = request.form["name"]
    price = request.form["price"]
    ingredient = Ingredient(name=name, price=price)
    try:
        db.session.add(ingredient)
        db.session.commit()
    except Exception as e:
        return "greska, vec postoji sastojak"

    return redirect("/ingredients")


@app.get("/ingredients/edit/<int:id>")
def show_edit_ingredient_form(id):
    ingredient = Ingredient.query.get(id)
    return render_template("pages/edit_ingredient.html", ingredient=ingredient)


@app.post("/ingredients/edit/<int:id>")
def edit_ingredient(id):
    old_ingredient = Ingredient.query.get(id)
    name = request.form["name"]
    price = request.form["price"]
    if old_ingredient.name != name:
        old_ingredient.name = name
    if old_ingredient.price != price:
        old_ingredient.price = price

    db.session.flush()
    db.session.commit()
    return redirect("/ingredients")


@app.get("/ingredients/delete/<int:id>")
def delete_ingredient(id):
    Ingredient.query.filter_by(id=id).delete()
    db.session.commit()
    return redirect("/ingredients")


# Recipes CRUD
@app.get("/recipes/<int:id>")
def show_recipe(id: int):
    recipe = Recipe.query.get(id)
    ingredients = []
    for ingredient in recipe.ingredients:
        ingr = Ingredient.query.get(ingredient.ingredient_id)
        ingr_dict = {
            "name": ingr.name,
            "price": ingr.price,
            "ammount": ingredient.ingredient_ammount,
        }
        ingredients.append(ingr_dict)
    data = {"recipe": recipe, "ingredients": ingredients}
    return render_template("pages/show_recipe.html", data=data)


@app.get("/recipes/new")
def create_recipe():
    ingredients = Ingredient.query.all()
    data = {"ingredients": ingredients}
    return render_template("pages/create_recipe.html", data=data)


@app.post("/recipes/new")
def create_recipe_in_db():
    recipe = Recipe(name=request.form["recipe_name"])
    ingredient_ids = list(map(int, request.form.getlist("ingredients")))
    ingredients = map(Ingredient.query.get, ingredient_ids)
    for ing in ingredients:
        assoc = RecipeIngredientAssoc()
        assoc.ingredient = ing
        assoc.ingredient_id = ing.id
        assoc.ingredient_name = ing.name
        assoc.ingredient_price = ing.price
        assoc.recipe = recipe
        assoc.recipe_id = recipe.id
        ammount = float(request.form[f"{ing.id}_ammount"])
        assoc.ingredient_ammount = ammount
    db.session.add(recipe)
    db.session.commit()
    return redirect("/recipes")


@app.get("/recipes/edit/<int:id>")
def edit_recipe_form(id):
    recipe = Recipe.query.get(id)
    ingredients_inside = []
    for ingredient in recipe.ingredients:
        ingr = Ingredient.query.get(ingredient.ingredient_id)
        ingr_dict = {
            "name": ingr.name,
            "price": ingr.price,
            "ammount": ingredient.ingredient_ammount,
            "id": ingr.id,
        }
        ingredients_inside.append(ingr_dict)
    all_ingredients = Ingredient.query.all()
    ingredients_rest = [
        ingr for ingr in all_ingredients if ingr not in ingredients_inside
    ]
    data = {
        "recipe": recipe,
        "ingredients_inside": ingredients_inside,
        "ingredients_rest": ingredients_rest,
    }
    return render_template("pages/edit_recipe.html", data=data)


@app.post("/recipes/edit/<int:id>")
def edit_recipe_in_db(id):
    old_recipe = Recipe.query.get(id)
    ingredient_ids = list(map(int, request.form.getlist("ingredients")))
    ingredients = map(Ingredient.query.get, ingredient_ids)
    ingredient_inside_ids = []

    for assoc in old_recipe.ingredients:
        ingredient_inside_ids.append(assoc.ingredient_id)
        if assoc.ingredient_id in ingredient_ids:
            ammount = int(request.form[f"{assoc.ingredient_id}_ammount"])
            assoc.ingredient_ammount = ammount
            db.session.add(assoc)

    for ing_id in ingredient_ids:
        if ing_id not in ingredient_inside_ids:
            assoc = RecipeIngredientAssoc()
            ing = Ingredient.query.get(ing_id)
            assoc.ingredient = ing
            assoc.ingredient_id = ing.id
            assoc.ingredient_name = ing.name
            assoc.ingredient_price = ing.price
            assoc.recipe = old_recipe
            assoc.recipe_id = old_recipe.id
            ammount = int(request.form[f"{ing.id}_ammount"])
            assoc.ingredient_ammount = ammount

    db.session.add(old_recipe)
    db.session.commit()
    return redirect("/recipes")


@app.route("/calculate", methods=["GET", "POST"])
def calculator_form():
    if request.method == "GET":
        data = {"recipes": Recipe.query.all()}
        return render_template("pages/calculate_form.html", data=data)
    elif request.method == "POST":
    ##    breakpoint()
        print("123")
        #data = {"recipes": Recipe.query.all()}
        req_data = request.form
        print(req_data)
        recipe_id = req_data["recipe_id"]
        recipe = Recipe.query.get(recipe_id)
        ingredients_inside = []
        ammount = req_data["ammount"]
        for ingredient in recipe.ingredients:
            ingr = Ingredient.query.get(ingredient.ingredient_id)
            ingr_dict = {
                "name": ingr.name,
                "price": ingr.price,
                "ammount": ingredient.ingredient_ammount,
                "calculated_ammount": float(ingredient.ingredient_ammount)
                * (int(ammount) / 10),
                "calculated_price": float(ingr.price) * (int(ammount) / 10),
                "id": ingr.id,
            }
            ingredients_inside.append(ingr_dict)
        price = calculate_price(ingredients_inside)
        data = {"ingredients": ingredients_inside, "price": price}
        ##breakpoint()
        return render_template("pages/calculate_results.html", data=data)


#@app.post("/calculate")
def calculate():
    breakpoint()
    req_data = request.form
    ##recipe_id = req_data["recipe_id"]
    recipe_id = int(req_data[0][1])
    recipe = Recipe.query.get(recipe_id)
    ingredients_inside = []
    ##ammount = req_data["ammount"]
    ammount = int(req_data[1][1])
    for ingredient in recipe.ingredients:
        ingr = Ingredient.query.get(ingredient.ingredient_id)
        ingr_dict = {
            "name": ingr.name,
            "price": ingr.price,
            "ammount": ingredient.ingredient_ammount,
            "calculated_ammount": float(ingredient.ingredient_ammount)
            * (int(ammount) / 10),
            "calculated_price": float(ingr.price) * (int(ammount) / 10),
            "id": ingr.id,
        }
        ingredients_inside.append(ingr_dict)
    price = calculate_price(ingredients_inside)
    data = {"ingredients": ingredients_inside, "price": price}

    #return jsonify(data)
    return data

def calculate_price(ingredients: list) -> float:
    price = 0
    for ingredient in ingredients:
        price += ingredient["calculated_price"]
    return price
